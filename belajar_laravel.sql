-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2019 at 03:15 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `pegawai_id` int(11) NOT NULL,
  `pegawai_nama` varchar(50) NOT NULL,
  `pegawai_jabatan` varchar(20) NOT NULL,
  `pegawai_umur` int(11) NOT NULL,
  `pegawai_alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`pegawai_id`, `pegawai_nama`, `pegawai_jabatan`, `pegawai_umur`, `pegawai_alamat`) VALUES
(1, 'Achmad JS', 'Siswa', 16, 'JL S.Parman GG Huni'),
(2, 'Farid MF', 'Siswa', 16, 'Babakan Bandung'),
(4, 'Joni', 'Web Designer', 25, 'Jl. Panglateh'),
(5, 'Tina Hasanah S.T.', 'autem', 27, 'Jr. Suryo Pranoto No. 816, Sukabumi 21128, Maluku'),
(6, 'Maria Widiastuti', 'vel', 39, 'Gg. Ujung No. 361, Padangpanjang 77361, DIY'),
(7, 'Mala Lalita Agustina S.Ked', 'accusamus', 33, 'Ki. Adisucipto No. 146, Padang 83715, SumSel'),
(8, 'Dinda Yulianti', 'delectus', 38, 'Psr. Basmol Raya No. 46, Banda Aceh 73545, Maluku'),
(9, 'Karimah Laksita', 'quas', 27, 'Ds. Siliwangi No. 231, Pekanbaru 16815, Papua'),
(10, 'Bakiman Dimaz Siregar M.TI.', 'est', 34, 'Jr. Babah No. 908, Bandar Lampung 27250, DIY'),
(11, 'Melinda Oktaviani', 'doloremque', 26, 'Ki. Dipenogoro No. 429, Bogor 78662, Lampung'),
(12, 'Eli Utami S.Kom', 'fuga', 25, 'Jr. S. Parman No. 92, Tidore Kepulauan 61296, SulTra'),
(13, 'Tiara Hassanah', 'quaerat', 39, 'Ds. Imam Bonjol No. 863, Lubuklinggau 50630, Riau'),
(14, 'Pia Padmasari S.IP', 'culpa', 40, 'Ds. Achmad Yani No. 50, Tanjung Pinang 56623, Riau'),
(15, 'Gandi Teddy Januar M.Farm', 'dolor', 29, 'Ki. Cikapayang No. 855, Pekalongan 47026, JaTeng'),
(16, 'Nasrullah Hasan Waluyo S.Sos', 'vel', 29, 'Jr. Sutan Syahrir No. 560, Denpasar 67225, Riau'),
(17, 'Eli Nurdiyanti', 'in', 25, 'Dk. Kebonjati No. 882, Surabaya 63876, Banten'),
(18, 'Ulva Haryanti', 'facere', 25, 'Jr. Setia Budi No. 100, Blitar 83538, Riau'),
(19, 'Tantri Lala Utami', 'veniam', 30, 'Ds. Gegerkalong Hilir No. 214, Langsa 18920, SulTeng'),
(20, 'Ani Usyi Andriani', 'consequatur', 33, 'Kpg. Moch. Ramdan No. 60, Lhokseumawe 75480, KalUt'),
(21, 'Paiman Latupono M.Farm', 'qui', 36, 'Jr. Sudirman No. 297, Tebing Tinggi 45667, KalSel'),
(22, 'Kanda Prabowo', 'ipsam', 37, 'Ds. Dipatiukur No. 549, Pasuruan 83018, KalUt'),
(23, 'Dariati Kurniawan S.IP', 'qui', 26, 'Ki. Hang No. 316, Dumai 87524, PapBar'),
(24, 'Ami Wastuti', 'excepturi', 35, 'Ds. Pacuan Kuda No. 43, Denpasar 35763, JaTim'),
(25, 'Mustika Digdaya Hidayat', 'ut', 38, 'Jln. Labu No. 369, Mataram 13958, JaBar'),
(26, 'Karya Kusumo', 'possimus', 39, 'Jr. Wora Wari No. 944, Tual 40118, DIY'),
(27, 'Jinawi Pranowo', 'in', 37, 'Dk. Pasirkoja No. 734, Cimahi 91022, Jambi'),
(28, 'Estiono Wijaya', 'non', 37, 'Kpg. Bambon No. 225, Mojokerto 60866, SulBar'),
(29, 'Mala Uyainah', 'consequatur', 31, 'Dk. Sukajadi No. 318, Sawahlunto 74670, KalBar'),
(30, 'Titi Kusmawati', 'dolore', 39, 'Ki. Bazuka Raya No. 273, Tegal 68058, KalTim'),
(31, 'Eman Januar', 'dolorem', 35, 'Ki. Daan No. 605, Semarang 61128, Banten'),
(32, 'Jatmiko Arta Maheswara', 'recusandae', 25, 'Jln. Bata Putih No. 854, Tebing Tinggi 18903, Papua'),
(33, 'Keisha Astuti', 'et', 37, 'Psr. Jend. Sudirman No. 813, Palu 16859, NTB'),
(34, 'Ayu Wahyuni', 'et', 37, 'Dk. Bagonwoto  No. 63, Medan 56801, SumUt'),
(35, 'Siska Farah Pertiwi S.T.', 'voluptatem', 27, 'Psr. Ters. Jakarta No. 358, Bandar Lampung 65336, Aceh'),
(36, 'Opan Galar Nugroho M.TI.', 'repellat', 27, 'Kpg. Sudirman No. 407, Tegal 59070, DKI'),
(37, 'Ridwan Winarno', 'ut', 28, 'Ki. Sutami No. 903, Administrasi Jakarta Timur 85581, Banten'),
(38, 'Yono Hakim', 'doloribus', 32, 'Kpg. Eka No. 805, Tanjung Pinang 41534, Bali'),
(39, 'Sari Janet Farida', 'enim', 27, 'Dk. Suryo No. 766, Batam 47112, PapBar'),
(40, 'Edi Iswahyudi M.Kom.', 'voluptatem', 33, 'Ki. Umalas No. 220, Prabumulih 54106, JaTim'),
(41, 'Rizki Tirta Maheswara', 'molestias', 28, 'Ds. Suprapto No. 703, Samarinda 13496, Maluku'),
(42, 'Cahya Saadat Prakasa M.Kom.', 'quia', 32, 'Kpg. Barat No. 56, Cilegon 81412, KalTeng'),
(43, 'Kasiran Siregar', 'nihil', 37, 'Ds. Achmad No. 366, Denpasar 54292, Papua'),
(44, 'Nilam Uyainah', 'expedita', 39, 'Kpg. Sugiono No. 179, Sungai Penuh 53476, SulSel'),
(45, 'Zelaya Kania Puspita M.TI.', 'nulla', 38, 'Jln. Jend. A. Yani No. 241, Sukabumi 98414, SumUt'),
(46, 'Pia Nurul Usamah', 'pariatur', 31, 'Jln. Cikapayang No. 855, Palopo 35511, JaTim'),
(47, 'Tiara Namaga', 'voluptas', 32, 'Gg. Yap Tjwan Bing No. 495, Pagar Alam 85131, SumUt'),
(48, 'Ajeng Pratiwi M.Ak', 'aut', 33, 'Jr. Tangkuban Perahu No. 561, Yogyakarta 28601, Lampung'),
(49, 'Samsul Bahuwarna Nainggolan', 'doloremque', 33, 'Gg. Ronggowarsito No. 228, Padangpanjang 93297, MalUt'),
(50, 'Genta Lintang Novitasari', 'ut', 30, 'Jr. Ekonomi No. 960, Tangerang 18576, Riau'),
(51, 'Nasab Hardi Maulana', 'nihil', 39, 'Kpg. Ciwastra No. 135, Kupang 93014, NTB'),
(52, 'Sadina Purwanti', 'nisi', 34, 'Kpg. Bayan No. 605, Administrasi Jakarta Pusat 37922, SulSel'),
(53, 'Laras Nasyiah', 'ullam', 27, 'Kpg. Daan No. 123, Solok 83856, Lampung'),
(54, 'Septi Raina Wastuti', 'aut', 28, 'Ds. Kartini No. 151, Pematangsiantar 47972, KalTim');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`pegawai_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `pegawai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
